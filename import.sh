#!/bin/bash
planning_folder=../planning/

for folder in *; do
  if [ -d "${folder}" ]; then
    # echo "$folder"
    cp $planning_folder${folder}*/*.html $folder/old
    last_file=$(ls $folder/old -1 | tail -1)
    # echo $last_file
    cp $folder/old/$last_file $folder/gantt.html
  fi
done
